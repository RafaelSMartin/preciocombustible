package com.rsmartin.fuelapp.remote.retrofit

object ApiUtils {

    val BASE_URL_GOB = "https://sedeaplicaciones.minetur.gob.es/ServiciosRESTCarburantes/PreciosCarburantes/"

    val apiService: Api
        get() = RetrofitClient.getClient(BASE_URL_GOB)!!.create(Api::class.java)
}