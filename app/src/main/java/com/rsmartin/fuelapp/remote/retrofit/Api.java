package com.rsmartin.fuelapp.remote.retrofit;

import com.rsmartin.fuelapp.remote.data.ApiDataGob.Model;
import retrofit2.Call;
import retrofit2.http.GET;

public interface Api {

    @GET("EstacionesTerrestres/")
    Call<Model> getStationsGob();

}
