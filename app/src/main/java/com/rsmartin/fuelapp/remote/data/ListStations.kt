package com.rsmartin.fuelapp.remote.data

import com.google.android.gms.maps.model.LatLng
import com.rsmartin.fuelapp.R
import java.io.Serializable

// POJO para cargar la lista del recycler vie con el listado de gasolineras, ordenadas por distancia o por precio
data class ListStations(
    var logo: Int,
    var name: String?, // list.attributes.rótulo hay que sacar el nombre de gasolinera de aqui
    var address: String?,
    var latlon: LatLng,
    var cp: String,
    var town: String, // municipio
    var stateTown: String, // state
    var distance: Int, // sacar la distancia lineal junto con LaLng
    var horary: String, // horario de apertura
    var price95: Double?,
    var price98: Any?,
    var priceGasOilA: Double?,
    var priceGasOilB: Any?,
    var priceNewGasOilA: Double?,

    var lastUpdate: String?, // Fecha de ultima llamada a la api
    var scalePrice: Int, // para determinar el color en que se mostrará el precio (rojo, naranja o verde)
    var scaleDistance: Int, // para determinar el color en que se mostrará la distancia (rojo, naranja o verde)
    var orderBy: ItemOrderBy? // para utilizar solo un adapter del recyclerview

) : Serializable {


    // se reutiliza el mismo layout para mostrar la lista tanto ordenada por lista como por precio. Se definen por tanto dos
    // tipos para poder saber por que se está ordenando
    enum class ItemOrderBy {
        DISTANCE, PRICE
    }

    companion object {

        fun mapperTypeStations(name: String): String {
            return when {
                name.toUpperCase().contains("GALP") -> "GALP"
                name.toUpperCase().contains("BP") -> "BP"
                name.toUpperCase().contains("CEPSA") -> "CEPSA"
                name.toUpperCase().contains("PLENOIL") -> "PLENOIL"
                name.toUpperCase().contains("CAMPSA") -> "CAMPSA"
                name.toUpperCase().contains("PETRONOR") -> "PETRONOR"
                name.toUpperCase().contains("REPSOL") -> "REPSOL"
                name.toUpperCase().contains("CARREFOUR") -> "CARREFOUR"
                name.toUpperCase().contains("EASYGAS") -> "EASYGAS"
                name.toUpperCase().contains("SHELL") -> "SHELL"
                name.toUpperCase().contains("EROSKI") -> "EROSKI"
                name.toUpperCase().contains("ALCAMPO") -> "ALCAMPO"
                name.toUpperCase().contains("ANDAMUR") -> "ANDAMUR"
                name.toUpperCase().contains("BALLENOIL") -> "BALLENOIL"
                name.toUpperCase().contains("AVIA") -> "AVIA"
                name.toUpperCase().contains("SARAS") -> "SARAS ENERGIA"
                name.toUpperCase().contains("EUROCAM") -> "EUROCAM"
                name.toUpperCase().contains("PREMIRA") -> "PREMIRA ENERGIA NORTE"
                name.toUpperCase().contains("ESTACIONES") -> "ESTACIONES GB"
                name.toUpperCase().contains("PETRONIEVES") -> "PETRONIEVES"
                name.toUpperCase().contains("ADS") -> "ADS"
                name.toUpperCase().contains("OION") -> "E.S. OION"
                name.toUpperCase().contains("TOCALU") -> "E.S. TOCALU S.L."
                else -> "OTRAS"
            }
        }

        fun mapperLogoStations(name: String): Int {
            return when {
                name.toUpperCase().contains("GALP") -> R.drawable.ic_logo_galp
                name.toUpperCase().contains("BP") -> R.drawable.ic_logo_bp
                name.toUpperCase().contains("CEPSA") -> R.drawable.ic_logo_cepsa
                name.toUpperCase().contains("PLENOIL") -> R.drawable.ic_logo_plenoil
                name.toUpperCase().contains("CAMPSA") -> R.drawable.ic_logo_campsa
                name.toUpperCase().contains("PETRONOR") -> R.drawable.ic_logo_petronor
                name.toUpperCase().contains("REPSOL") -> R.drawable.ic_logo_repsol
                name.toUpperCase().contains("CARREFOUR") -> R.drawable.ic_logo_carrefour
                name.toUpperCase().contains("SHELL") -> R.drawable.ic_logo_shell
                name.toUpperCase().contains("EROSKI") -> R.drawable.ic_logo_eroski
                name.toUpperCase().contains("ANDAMUR") -> R.drawable.ic_logo_andamur
                name.toUpperCase().contains("BALLENOIL") -> R.drawable.ic_logo_ballenoil
                name.toUpperCase().contains("ALCAMPO") -> R.drawable.ic_logo_alcampo
                name.toUpperCase().contains("EUROCAM") -> R.drawable.ic_logo_eurocam
                name.toUpperCase().contains("AVIA") -> R.drawable.ic_logo_avia
                name.toUpperCase().contains("EASYGAS") -> R.drawable.ic_logo_easygas
                name.toUpperCase().contains("SARAS") -> R.drawable.ic_logo_saras
                else -> R.drawable.ic_local_gas_station_black_48dp
            }
        }

        fun mapperMarkerStations(name: String): Int {
            return when {
                name.toUpperCase().contains("GALP") -> R.drawable.marker_galp
                name.toUpperCase().contains("BP") -> R.drawable.marker_bp
                name.toUpperCase().contains("CEPSA") -> R.drawable.marker_cepsa
                name.toUpperCase().contains("PLENOIL") -> R.drawable.marker_plenoil
                name.toUpperCase().contains("CAMPSA") -> R.drawable.marker_campsa
                name.toUpperCase().contains("PETRONOR") -> R.drawable.marker_petronor
                name.toUpperCase().contains("REPSOL") -> R.drawable.marker_repsol
                name.toUpperCase().contains("CARREFOUR") -> R.drawable.marker_carrefour
                name.toUpperCase().contains("SHELL") -> R.drawable.marker_shell
                name.toUpperCase().contains("EROSKI") -> R.drawable.marker_eroski
                name.toUpperCase().contains("ANDAMUR") -> R.drawable.marker_andamur
                name.toUpperCase().contains("BALLENOIL") -> R.drawable.marker_ballenoil
                name.toUpperCase().contains("ALCAMPO") -> R.drawable.marker_alcampo
                name.toUpperCase().contains("EUROCAM") -> R.drawable.marker_eurocam
                name.toUpperCase().contains("AVIA") -> R.drawable.marker_avia
                name.toUpperCase().contains("EASYGAS") -> R.drawable.marker_easygas
                name.toUpperCase().contains("SARAS") -> R.drawable.marker_saras
                else -> R.drawable.marker_gas_station_default
            }
        }

    }

}