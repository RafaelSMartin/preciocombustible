package com.rsmartin.fuelapp.config

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import com.rsmartin.fuelapp.R
import com.rsmartin.fuelapp.Utils.SharedPreference
import com.rsmartin.fuelapp.home.HomeActivity
import kotlinx.android.synthetic.main.activity_config.*

class DistancePreferenceActivity : AppCompatActivity() {

    private var list_distance = arrayOf("5 km", "10 Km", "20 km", "30 Km", "50 kM", "100 Km")

    private var savedDistance: String = ""
    private var savedDistancePosition: Int = 0

    private lateinit var sharedPreference: SharedPreference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_distance_preference)
        setSupportActionBar(toolbar)

        sharedPreference = SharedPreference(this)

        initSpinnerDistancia()
        initNextButton()

    }

    private fun initNextButton() {
        var next: Button = findViewById(R.id.next)

        next.setOnClickListener {
            sharedPreference.save("distanceConfig", savedDistance)
            sharedPreference.save("distancePositionConfig", savedDistancePosition)
            // ir a home Activity
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            this.finish()
        }
    }


    private fun initSpinnerDistancia() {
        val spinnerDistancia: Spinner = findViewById(R.id.sp_distancia)
        val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, list_distance)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerDistancia.adapter = arrayAdapter

        spinnerDistancia.setSelection(sharedPreference.getValueInt("distancePositionConfig"))

        spinnerDistancia.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                savedDistance = list_distance[position]
                savedDistancePosition = position
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }

        }
    }

}
