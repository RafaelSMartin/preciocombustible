package com.rsmartin.fuelapp.config

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import com.rsmartin.fuelapp.R
import com.rsmartin.fuelapp.Utils.SharedPreference
import com.rsmartin.fuelapp.home.HomeActivity
import kotlinx.android.synthetic.main.activity_config.*

class GasPreferenceActivity : AppCompatActivity() {

    private var list_types_gasoil = arrayOf("Gasolina 95", "Gasolina 98", "Diésel", "Diésel Mejorado", "Gasóleo B")

    private var savedCombustible: String = ""
    private var savedCombustiblePosition: Int = 0

    private lateinit var sharedPreference: SharedPreference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gas_preference)
        setSupportActionBar(toolbar)

        sharedPreference = SharedPreference(this)

        initSpinnerCombustible()
        initNextButton()

    }

    private fun initNextButton() {
        var next: Button = findViewById(R.id.next)

        next.setOnClickListener {
            sharedPreference.save("combustibleConfig", savedCombustible)
            sharedPreference.save("combustiblePositionConfig", savedCombustiblePosition)
            // ir a home Activity
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            this.finish()
        }
    }

    private fun initSpinnerCombustible() {
        val spinnerCombustible: Spinner = findViewById(R.id.sp_combustible)
        val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, list_types_gasoil)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCombustible.adapter = arrayAdapter

        spinnerCombustible.setSelection(sharedPreference.getValueInt("combustiblePositionConfig"))

        spinnerCombustible.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                savedCombustible = list_types_gasoil[position]
                savedCombustiblePosition = position
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }

        }
    }
}
