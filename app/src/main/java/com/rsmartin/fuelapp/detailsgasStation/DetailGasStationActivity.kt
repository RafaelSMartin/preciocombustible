package com.rsmartin.fuelapp.detailsgasStation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.widget.TextViewCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.TextView
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.rsmartin.fuelapp.R
import com.rsmartin.fuelapp.Utils.SharedPreference
import com.rsmartin.fuelapp.maps.MapsActivity
import com.rsmartin.fuelapp.remote.data.ListStations
import kotlinx.android.synthetic.main.activity_detail_gas_station.*

class DetailGasStationActivity : AppCompatActivity() {
    private lateinit var sharedPreference: SharedPreference

    private lateinit var mAdView: AdView

    companion object {
        fun newIntent(context: Context?, itemDto: ListStations, location: String): Intent {
            val intent = Intent(context, DetailGasStationActivity::class.java)
            intent.putExtra("GAS_NAME", itemDto.name)
            intent.putExtra("GAS_ADDRESS", itemDto.address)
            intent.putExtra("GAS_LOCATION", location)
            intent.putExtra("GAS_95", String.format("%.3f", itemDto.price95))
            intent.putExtra("GAS_98", itemDto.price98.toString())
            intent.putExtra("GAS_A", String.format("%.3f", itemDto.priceGasOilA))
            intent.putExtra("GAS_A_NEW", String.format("%.3f", itemDto.priceNewGasOilA))
            intent.putExtra("GAS_B", itemDto.priceGasOilB.toString())
            intent.putExtra("HORARIO", itemDto.horary)
            intent.putExtra("LATITUDE", itemDto.latlon.latitude)
            intent.putExtra("LONGITUDE", itemDto.latlon.longitude)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_gas_station)



        sharedPreference = SharedPreference(this)

        // Obtenemos info de la gasolinera
        val gasName = intent.getStringExtra("GAS_NAME")
        val gasAdress = intent.getStringExtra("GAS_ADDRESS")
        val gasLocation = intent.getStringExtra("GAS_LOCATION")
        val gasPrice95 = intent.getStringExtra("GAS_95")
        val gasPrice98 = intent.getStringExtra("GAS_98")
        val gasPriceA = intent.getStringExtra("GAS_A")
        val gasPriceANew = intent.getStringExtra("GAS_A_NEW")
        val gasPriceB = intent.getStringExtra("GAS_B")
        val horario = intent.getStringExtra("HORARIO")
        val latitude = intent.getDoubleExtra("LATITUDE", 0.0)
        val longitude = intent.getDoubleExtra("LONGITUDE", 0.0)


        detailLogo.setImageResource(
            ListStations.mapperLogoStations(
                gasName
            )
        )
        detailName.text = gasName
        detailAddress.text = gasAdress
        detailLocation.text = gasLocation
        // Si me vienen a null los parametros escondo su vista.
        setDataIntoViews(detailGas95, gasPrice95, "Gasolina 95:")
        setDataIntoViews(detailGas98, gasPrice98, "Gasolina 98:")
        setDataIntoViews(detailGasA, gasPriceA, "Diésel:")
        setDataIntoViews(detailGasANew, gasPriceANew, "Diésel Mejorado:")
        setDataIntoViews(detailGasB, gasPriceB, "Gasóleo B:")
        detailHorario.text = horario

        // Se marca en bold el tipo de gasolina en función del valor de preferencias
        val pref_type_gas = sharedPreference.getValueInt("combustiblePositionConfig")
        when (pref_type_gas) {
            0 -> { TextViewCompat.setTextAppearance(detailGas95, R.style.TextDetailGas)}
            1 -> { TextViewCompat.setTextAppearance(detailGas98, R.style.TextDetailGas) }
            2 -> { TextViewCompat.setTextAppearance(detailGasA, R.style.TextDetailGas) }
            3 -> { TextViewCompat.setTextAppearance(detailGasANew, R.style.TextDetailGas) }
            4 -> { TextViewCompat.setTextAppearance(detailGasB, R.style.TextDetailGas) }
            else -> {
            }
        }

        btnToMap.setOnClickListener{
            // coordenadas en latitude y longitude
            val intent = MapsActivity.newIntent(this, latitude, longitude)
            startActivity(intent)
            this.finish()
        }

        // Botón favoritos. Quitar si no se implementa
//        val toggleButton = findViewById<ToggleButton>(R.id.toggleStart)
//        toggleButton?.setOnCheckedChangeListener { buttonView, isChecked ->
//            val msg = if (isChecked) "Añadida a favoritos" else "Borrada de favoritos"
//            Toast.makeText(this@DetailGasStationActivity, msg, Toast.LENGTH_SHORT).show()
//            if (isChecked)
//                //toggleButton.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_star_black_48dp));
//            else
//                //toggleButton.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_star_border_black_48dp));
//        }

        initAdView()

    }

    fun setDataIntoViews(view: TextView, data: String, dataLabel: String) {
        if (data != null && data != "nul") {
            view.visibility = View.VISIBLE
            view.text = dataLabel + " " + data + "€/l"
        } else {
            view.visibility = View.GONE
        }
    }

    private fun initAdView() {
        mAdView = findViewById(R.id.adView) as AdView
        val adRequest = AdRequest.Builder()
            .addTestDevice("680CC25C7711DEE7C2B02AB27B705D4D")
            .build()
        adView.loadAd(adRequest)
    }
}