package com.rsmartin.fuelapp.adapter

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.rsmartin.fuelapp.R
import com.rsmartin.fuelapp.remote.data.ListStations
import kotlinx.android.synthetic.main.activity_home.view.*


class ListAdapter(
    private val items: List<ListStations>,
    val clickListener: (ListStations, Int, View) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private val TYPE_DISTANCE = 1
        private val TYPE_PRICE = 2

        // Poner categoría de precio/distancia: alto (rojo), medio (naranja), bajo (verde)
        private val HIGH = 2
        private val MEDIUM = 1
        private val LOW = 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, i: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.element_list_order_by, parent, false)
        return ListViewHolder(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ListViewHolder).bind(items[position], clickListener)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        val list = items[position]
        return when {
            list.orderBy == ListStations.ItemOrderBy.DISTANCE -> TYPE_DISTANCE
            list.orderBy == ListStations.ItemOrderBy.PRICE -> TYPE_PRICE
            else -> -1
        }
    }

    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var logo: ImageView
        var name: TextView
        var priceOrDistance: TextView
        var distanceOrPrice: TextView
        var address: TextView

//        var lastUpdate: TextView
//        var ivMap: ImageView


        init {
            logo = itemView.findViewById<View>(R.id.logo) as ImageView
            name = itemView.findViewById<View>(R.id.name) as TextView
            priceOrDistance = itemView.findViewById<View>(R.id.priceOrDistance) as TextView
            distanceOrPrice = itemView.findViewById<View>(R.id.distanceOrPrice) as TextView
            address = itemView.findViewById<View>(R.id.address) as TextView

//            lastUpdate = itemView.findViewById<View>(R.id.lastUpdate) as TextView
//            ivMap = itemView.findViewById<View>(R.id.iVMap) as ImageView

        }

        fun bind(item: ListStations, clickListener: (ListStations, Int, View) -> Unit) {
            logo.setImageResource(item.logo)
            name.text = item.name

            // Depedendiendo del tipo
            when (itemViewType) {
                TYPE_DISTANCE -> {
                    priceOrDistance.text = item.price95.toString() + "€/l"
                    distanceOrPrice.text = item.distance.toString() + "Km"
                }
                TYPE_PRICE -> {
                    distanceOrPrice.text = item.price95.toString() + "€/l"
                    priceOrDistance.text = item.distance.toString() + "Km"
                }
                else -> {
                }
            }
            address.text = item.address

//            lastUpdate.text = item.lastUpdate
//            ivMap.setImageResource(R.drawable.ic_logo_map)


            val context = logo.context // cogemos el contexto de cualquier vista
            val priceInScale = item.scalePrice
            val distanceInScale = item.scaleDistance

            // mostrar el color según la escala de precios que le corresponde
            when (priceInScale) {
                HIGH -> priceOrDistance.setTextColor(ContextCompat.getColor(context, R.color.colorHighValue))
                MEDIUM -> priceOrDistance.setTextColor(ContextCompat.getColor(context, R.color.colorMediumValue))
                LOW -> priceOrDistance.setTextColor(ContextCompat.getColor(context, R.color.colorLowValue))
            }

            when (distanceInScale) {
                HIGH -> distanceOrPrice.setTextColor(ContextCompat.getColor(context, R.color.colorHighValue))
                MEDIUM -> distanceOrPrice.setTextColor(ContextCompat.getColor(context, R.color.colorMediumValue))
                LOW -> distanceOrPrice.setTextColor(ContextCompat.getColor(context, R.color.colorLowValue))
            }
            //Listener en el item entero para ir al detalle
            itemView.setOnClickListener { clickListener(item, adapterPosition, itemView.logo) }
        }
    }

}