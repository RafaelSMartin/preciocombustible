package com.rsmartin.fuelapp.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.rsmartin.fuelapp.tabsOrder.TabOrderByDistanceFragment
import com.rsmartin.fuelapp.tabsOrder.TabOrderByPriceFragment


// Clase para gestionar los TABS para ordenar por Distancia / Precio
class MyPagerTabAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                TabOrderByDistanceFragment()
            }
            else -> {
                return TabOrderByPriceFragment()
            }
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "DISTANCIA"
            else -> {
                return "PRECIO"
            }
        }
    }

}