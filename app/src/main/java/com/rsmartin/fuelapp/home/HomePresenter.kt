package com.rsmartin.fuelapp.home

import com.rsmartin.fuelapp.remote.data.ApiDataGob.Model
import com.rsmartin.fuelapp.remote.retrofit.Api
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomePresenter(var homeView: HomeView?) {

    fun getOilsGob(mAPIService: Api) {

        homeView!!.showProgress()
        val call = mAPIService.stationsGob

        call.enqueue(object : Callback<Model> {
            override fun onResponse(call: Call<Model>, response: Response<Model>) {
                val model = response.body()
//                Log.d("ResponseString", model?.listaEESSPrecio?.size.toString())//10309

                if (response.isSuccessful) {
                    //Log.d("ResponseStringSuccesful", model?.listaEESSPrecio.toString())
                    homeView?.saveModelGob(model)
                }
                homeView!!.hideProgress()
            }

            override fun onFailure(call: Call<Model>?, t: Throwable?) {
//                Log.v("ErrorRetrofit", t.toString())
                homeView!!.hideProgress()
            }
        })
    }

}