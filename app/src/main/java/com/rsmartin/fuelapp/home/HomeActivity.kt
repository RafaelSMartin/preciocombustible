package com.rsmartin.fuelapp.home

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.rsmartin.fuelapp.R
import com.rsmartin.fuelapp.maps.MapsActivity
import com.rsmartin.fuelapp.remote.data.ApiDataGob.ListaEESSPrecio
import com.rsmartin.fuelapp.remote.data.ApiDataGob.Model
import com.rsmartin.fuelapp.remote.retrofit.Api
import com.rsmartin.fuelapp.remote.retrofit.ApiUtils

class HomeActivity : AppCompatActivity(), HomeView {

    companion object {
        var listaHomeGob: List<ListaEESSPrecio> = ArrayList()
    }

    private var mAPIService: Api? = null

    private val presenter = HomePresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        mAPIService = ApiUtils.apiService

        presenter.getOilsGob(mAPIService!!)

    }

    override fun saveModelGob(model: Model?) {
        //Guardar el modelo en DB
        listaHomeGob = model?.listaEESSPrecio as List<ListaEESSPrecio>
        launchGoogleMaps()
    }

    override fun launchGoogleMaps() {
        val intent = Intent(this, MapsActivity::class.java)
        startActivity(intent)
        this.finish()
    }

    override fun showProgress(){
    }

    override fun hideProgress(){
    }


}
