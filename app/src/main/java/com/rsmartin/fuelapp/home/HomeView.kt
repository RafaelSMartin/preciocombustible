package com.rsmartin.fuelapp.home

import com.rsmartin.fuelapp.remote.data.ApiDataGob.Model

interface HomeView {
    fun showProgress()
    fun hideProgress()
    fun saveModelGob(model: Model?)
    fun launchGoogleMaps()
}