package com.rsmartin.fuelapp.tabsOrder


import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rsmartin.fuelapp.R
import com.rsmartin.fuelapp.adapter.MyPagerTabAdapter



class TabsOrderByFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        val rootView = inflater.inflate(R.layout.tabhost_orderby, container, false)

        tabLayout = rootView.findViewById(R.id.tabs_main) as TabLayout

        viewPager = rootView.findViewById(R.id.viewpager_main) as ViewPager

        viewPager.adapter = MyPagerTabAdapter(childFragmentManager)

        tabLayout.post(Runnable { tabLayout.setupWithViewPager(viewPager) })

        return rootView
    }

    companion object{
        lateinit var tabLayout : TabLayout
        lateinit var viewPager: ViewPager
        var items = 2
    }



}


