package com.rsmartin.fuelapp.tabsOrder

import android.app.ActivityOptions
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.model.LatLng
import com.rsmartin.fuelapp.R
import com.rsmartin.fuelapp.Utils.SharedPreference
import com.rsmartin.fuelapp.Utils.Utils
import com.rsmartin.fuelapp.adapter.ListAdapter
import com.rsmartin.fuelapp.detailsgasStation.DetailGasStationActivity
import com.rsmartin.fuelapp.home.HomeActivity
import com.rsmartin.fuelapp.remote.data.ListStations
import com.rsmartin.fuelapp.remote.data.ListStations.Companion.mapperLogoStations
import com.rsmartin.fuelapp.remote.data.ListStations.Companion.mapperTypeStations
import kotlin.math.roundToInt

class TabOrderByPriceFragment: Fragment() {

    private var recyclerD: RecyclerView? = null
    private var adapterD: RecyclerView.Adapter<*>? = null
    private var lManagerD: RecyclerView.LayoutManager? = null
    lateinit var sharedPreference: SharedPreference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.activity_tab_list, container, false)
        sharedPreference = SharedPreference(rootView.context)

        /******************* IMPORTANTE *************************/
        //Inicializar los elementos (SEGUN API). Si no existe logo cargar por defecto ic_gas material design icon
        val items = ArrayList<ListStations>()
        val aux = HomeActivity.listaHomeGob

        var auxPrecioNuevoGasoleoA = 0.00
        var auxPrecioGasoleoA = 0.00
        var auxPrecioGasolina95ProtecciN = 0.00

        for (list in aux) {
            if (list.latitud != null && list.longitud != null) {

                if (list.precioNuevoGasoleoA != null) {
                    auxPrecioNuevoGasoleoA = Utils.replaceComaToDot(list.precioNuevoGasoleoA)
                }
                if (list.precioGasolina95ProtecciN != null) {
                    auxPrecioGasolina95ProtecciN = Utils.replaceComaToDot(list.precioGasolina95ProtecciN)
                }
                if (list.precioGasoleoA != null) {
                    auxPrecioGasoleoA = Utils.replaceComaToDot(list.precioGasoleoA)
                }

                items.add(
                    ListStations(
                        mapperLogoStations(list.rotulo),
                        mapperTypeStations(list.rotulo),
                        list.direcciN,
                        LatLng(Utils.replaceComaToDot(list.latitud), Utils.replaceComaToDot(list.longitud)),
                        list.cp,
                        list.provincia,
                        list.provincia,
                        Utils.distance(
                            Utils.replaceComaToDot(list.latitud),
                            Utils.replaceComaToDot(list.longitud),
                            sharedPreference.getValueDouble("CurrentLat"),
                            sharedPreference.getValueDouble("CurrentLon")
                        ).roundToInt(),
                        list.horario,
                        auxPrecioGasolina95ProtecciN,
                        list.precioGasolina98,
                        auxPrecioGasoleoA,
                        list.precioGasoleoB,
                        auxPrecioNuevoGasoleoA,

                        "",
                        1,
                        0,
                        ListStations.ItemOrderBy.PRICE
                    )
                )
                auxPrecioNuevoGasoleoA = 0.00
                auxPrecioGasoleoA = 0.00
                auxPrecioGasolina95ProtecciN = 0.00
            }
        }
        lateinit var orderItemsByPrice: List<ListStations>
        val pref_type_gas = sharedPreference.getValueInt("combustiblePositionConfig")
        when (pref_type_gas) {
            0 -> {
                orderItemsByPrice = items.sortedWith(compareBy(ListStations::price95))
            }
            //1 -> {  orderItemsByPrice = items.sortedWith(compareBy (ListStations::price98)) }
            2 -> {
                orderItemsByPrice = items.sortedWith(compareBy(ListStations::priceGasOilA))
            }
            3 -> {
                orderItemsByPrice = items.sortedWith(compareBy(ListStations::priceNewGasOilA))
            }
            //4 -> {  orderItemsByPrice = items.sortedWith(compareBy (ListStations::priceGasOilB)) }
        }




        // Obtener el Recycler
        recyclerD = rootView.findViewById<View>(R.id.recyclerList) as RecyclerView
        recyclerD!!.setHasFixedSize(true)

        // Usar un administrador para LinearLayout
        lManagerD = LinearLayoutManager(context)
        recyclerD!!.layoutManager = lManagerD

        // Crear un nuevo adaptador
        //adapterD = ListAdapter(items)
        //recyclerD!!.adapter = adapterD

        // adaptador recycler que incluye el onclick listener con la transicion del logo
        recyclerD?.adapter = ListAdapter(orderItemsByPrice) { itemDto: ListStations, position: Int, v: View ->
            //intent.putExtra("INFO_GAS_STATION", itemDto)

            var location = itemDto.cp + ", " + itemDto.town + ", " + itemDto.stateTown

            val intent = DetailGasStationActivity.newIntent(context, itemDto, location)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val options = ActivityOptions.makeSceneTransitionAnimation(activity,
                    Pair.create<View, String>(v, getString(R.string.transition_name_img)))
                startActivity(intent, options.toBundle())
            }
            else {
                startActivity(intent)
            }
        }

        return rootView
    }

}