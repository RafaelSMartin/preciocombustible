package com.rsmartin.fuelapp.tabsOrder

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.rsmartin.fuelapp.R
import com.rsmartin.fuelapp.adapter.MyPagerTabAdapter
import kotlinx.android.synthetic.main.tabhost_orderby.*

// Carga la activiy con el tab: DIASTANCIA y PRECIO
class TabsOrderByActivity : AppCompatActivity() {


    companion object {
        const val EXTRA_LIST = "extra_list"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tabhost_orderby)

        val fragmentAdapter = MyPagerTabAdapter(supportFragmentManager)

        viewpager_main.adapter = fragmentAdapter

        tabs_main.setupWithViewPager(viewpager_main)

    }


}