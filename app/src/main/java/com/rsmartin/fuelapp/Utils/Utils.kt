package com.rsmartin.fuelapp.Utils

import android.support.annotation.IdRes
import android.support.v7.app.AppCompatActivity
import java.text.SimpleDateFormat

class Utils {
    companion object{

        fun replaceComaToDot(s: String): Double{
            val final =  "${s.replace(",", ".")}"
            return final.toDouble()
        }

        fun getNormalDate(timeInMillies: Long): String? {
            var date: String?
            val formatter = SimpleDateFormat("dd/MM/yyyy")
            date = formatter.format(timeInMillies)
            println("Today is " + date!!)
            return date
        }

        fun distance(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Float {
            val EARTH_RADIUS_M = 6371000
            val dLat = Math.toRadians(lat2 - lat1)
            val dLon = Math.toRadians(lon2 - lon1)
            val dLat1 = Math.toRadians(lat1)
            val dLat2 = Math.toRadians(lat2)

            val a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                    Math.sin(dLon / 2) * Math.sin(dLon / 2) *
                    Math.cos(dLat1) * Math.cos(dLat2)
            val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
            return ((c * EARTH_RADIUS_M) / 1000).toFloat()
        }

        fun AppCompatActivity.replaceFragmenty(fragment: android.support.v4.app.Fragment,
                                               allowStateLoss: Boolean = false,
                                               @IdRes containerViewId: Int) {
            val ft = supportFragmentManager
                .beginTransaction()
                .replace(containerViewId, fragment)
            if (!supportFragmentManager.isStateSaved) {
                ft.commit()
            } else if (allowStateLoss) {
                ft.commitAllowingStateLoss()
            }
        }

    }




}