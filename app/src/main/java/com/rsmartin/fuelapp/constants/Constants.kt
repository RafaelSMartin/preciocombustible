package com.rsmartin.fuelapp.constants

class Constants {
    companion object {
        val ID_BANNER_APLICATION = "ca-app-pub-5611794729293328~7804426188"
        val ID_BANNER = "ca-app-pub-5611794729293328/8211620290"
        val ID_BANNER_TEST = "ca-app-pub-3940256099942544/6300978111"
    }
}