package com.rsmartin.fuelapp.notifications

import android.app.Application
import com.rsmartin.fuelapp.R

class MyApplication : Application() {

    lateinit var myAppNotificationManager: MyAppNotificationManager
    override fun onCreate() {
        super.onCreate()
        myAppNotificationManager = MyAppNotificationManager.getInstance(this);
        myAppNotificationManager.registerNotificationChannel(getString(R.string.NEWS_CHANNEL_ID),
                getString(R.string.CHANNEL_NEWS),getString(R.string.CHANNEL_DESCRIPTION))
    }
}