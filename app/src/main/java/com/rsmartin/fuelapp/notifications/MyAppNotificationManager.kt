package com.rsmartin.fuelapp.notifications

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import com.rsmartin.fuelapp.R

class MyAppNotificationManager {
    var context: Context

    constructor(context: Context){
        this.context = context
    }

    companion object {
        private lateinit var instance: MyAppNotificationManager
        fun getInstance(context: Context): MyAppNotificationManager {
            instance = MyAppNotificationManager(context)
            return instance
        }
    }

    fun registerNotificationChannel(channelId:String, channelName:String, channelDescription:String){
        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.O){
            val channel = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT)
            channel.description=channelDescription
            channel.setShowBadge(true)

            val manager = context.getSystemService(NotificationManager::class.java)
            manager.createNotificationChannel(channel);
        }
    }

    fun triggerNotification(targetActivity: Class<out Any> , channelId:String, title:String, text:String, bigText:String, priority:Int, autoCancel:Boolean, notificationId:Int){
        val intent = Intent(context, targetActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val pendingIntent = PendingIntent.getActivity(context, 0, intent,0)

        val notificationBuilder = NotificationCompat.Builder(context, channelId).apply {
            setSmallIcon(R.drawable.ic_notification)
            setLargeIcon(BitmapFactory.decodeResource(context.resources,R.drawable.ic_icon_large))
            setContentTitle("Novedades Disponibles")
            setContentText("Se han realizado cambios dentro de los precios que más te convienen")
            setPriority(NotificationCompat.PRIORITY_DEFAULT)
            setStyle(NotificationCompat.BigTextStyle().bigText("Novedades que van a ser un ahorro para tu bolsillo"))
            setContentIntent(pendingIntent)
            setChannelId(channelId)
            setAutoCancel(true)
        }

        val notificationManager = NotificationManagerCompat.from(context)
        notificationManager.notify(notificationId, notificationBuilder.build())

    }
}