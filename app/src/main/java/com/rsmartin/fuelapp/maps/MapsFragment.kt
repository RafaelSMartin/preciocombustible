package com.rsmartin.fuelapp.maps

import android.annotation.SuppressLint
import android.location.Location
import android.os.Bundle
import android.support.v4.app.Fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.rsmartin.fuelapp.R
import com.rsmartin.fuelapp.Utils.SharedPreference
import com.rsmartin.fuelapp.Utils.Utils
import com.rsmartin.fuelapp.detailsgasStation.DetailGasStationActivity
import com.rsmartin.fuelapp.home.HomeActivity
import com.rsmartin.fuelapp.remote.data.ApiDataGob.ListaEESSPrecio
import com.rsmartin.fuelapp.remote.data.ListStations


class MapsFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    companion object {
        var listHomeFilteredByDistance: ArrayList<ListaEESSPrecio> = ArrayList()

        fun newInstance(lat: Double, lon: Double): MapsFragment {
            val fragment = MapsFragment()
            val args = Bundle()
            args.putDouble("LATITUDE", lat)
            args.putDouble("LONGITUDE", lon)
            fragment.arguments = args
            return fragment
        }
    }

    private fun receiveExtrasFromMapsActivity() {
        val args = arguments
        if (args != null) {
            latitude = args.getDouble("LATITUDE", 0.0)
            longitude = args.getDouble("LONGITUDE", 0.0)
        }
    }

    private val presenter = MapsPresenter(this)

    private lateinit var mMap: GoogleMap
    private lateinit var listHomeGob: List<ListaEESSPrecio>
    private lateinit var marker: Marker

    private lateinit var sharedPreference: SharedPreference

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private lateinit var currentLatLng: LatLng

    private var latitude = 0.0
    private var longitude = 0.0


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Get the custom view for this fragment layout
        val view = inflater!!.inflate(R.layout.content_maps, container, false)

        receiveExtrasFromMapsActivity()

        listHomeGob = HomeActivity.listaHomeGob
        sharedPreference = SharedPreference(view.context)

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(view.context)

        mapFragment?.getMapAsync(this)

        return view
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.uiSettings.isCompassEnabled
        mMap.uiSettings.isZoomControlsEnabled = true
        mMap.isMyLocationEnabled = true

        mMap.setOnMarkerClickListener(this)

        fusedLocationClient.lastLocation.addOnSuccessListener { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {
                lastLocation = location
                currentLatLng = LatLng(location.latitude, location.longitude)

                sharedPreference.save("CurrentLat", location.latitude.toFloat())
                sharedPreference.save("CurrentLon", location.longitude.toFloat())

                if (latitude != 0.0 && longitude != 0.0) {
                    var customLatLon = LatLng(latitude, longitude)
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(customLatLon, 12f))
                } else {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
                }
            }
        }

        addMarkers(
            mMap, LatLng(
                sharedPreference.getValueDouble("CurrentLat"),
                sharedPreference.getValueDouble("CurrentLon")
            )
        )

    }

    override fun onMarkerClick(marker: Marker?): Boolean {
        if (marker != null) {
            val station = presenter.getDetailStation(marker.position.latitude, marker.position.longitude, listHomeGob)
            val location = station.cp + ", " + station.town + ", " + station.stateTown
            val intent = DetailGasStationActivity.newIntent(this@MapsFragment.context, station, location)
            requireActivity().startActivity(intent)
            return true
        } else {
            return false
        }
    }

    private fun addMarkers(mMap: GoogleMap, currentLatLng: LatLng) {
        var listLaLng: LatLng
        var i = 0

        for (listAux in listHomeGob) {

            if ((listAux.latitud) != null || (listAux.longitud) != null) {

                listLaLng = LatLng(Utils.replaceComaToDot(listAux.latitud), Utils.replaceComaToDot(listAux.longitud))

                if (Utils.distance(
                        currentLatLng.latitude,
                        currentLatLng.longitude,
                        listLaLng.latitude,
                        listLaLng.longitude
                    ) <=
                    sharedPreference.getValueString("distanceConfig")!!.removeSuffix("kM").trim().toInt()
                ) {

                    listHomeFilteredByDistance.add(listAux)

                    marker = mMap.addMarker(MarkerOptions()
                        .position(listLaLng)
                        .icon(BitmapDescriptorFactory.fromResource(ListStations.mapperMarkerStations(listAux.rotulo)))
                    )
                    i++
                }
            }
        }
    }

}





