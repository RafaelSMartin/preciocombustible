package com.rsmartin.fuelapp.maps

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.ActivityCompat
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle

import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.rsmartin.fuelapp.R
import com.rsmartin.fuelapp.Utils.RateMyApp
import com.rsmartin.fuelapp.Utils.SharedPreference
import com.rsmartin.fuelapp.Utils.Utils.Companion.replaceFragmenty
import com.rsmartin.fuelapp.config.DistancePreferenceActivity
import com.rsmartin.fuelapp.config.GasPreferenceActivity

import com.rsmartin.fuelapp.home.HomeActivity
import com.rsmartin.fuelapp.remote.data.ApiDataGob.ListaEESSPrecio

import com.rsmartin.fuelapp.tabsOrder.TabsOrderByFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*

class MapsActivity : AppCompatActivity(), MapsView, NavigationView.OnNavigationItemSelectedListener {

    //private val presenter = MapsPresenter(this)

    private lateinit var mMap: GoogleMap
    private lateinit var lastLocation: Location
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var listHomeGob: List<ListaEESSPrecio>
    private lateinit var marker: Marker
    private lateinit var sharedPreference: SharedPreference

    private var latitude = 0.0
    private var longitude = 0.0

    private val EXTRA_LIST = "extra_list"

    companion object {
        const val LOCATION_PERMISSION_REQUEST_CODE = 1

        fun newIntent(context: Context, lat: Double, lon: Double): Intent {
            val intent = Intent(context, MapsActivity::class.java)
            intent.putExtra("LATITUDE", lat)
            intent.putExtra("LONGITUDE", lon)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        setSupportActionBar(toolbar)

        listHomeGob = HomeActivity.listaHomeGob
        sharedPreference = SharedPreference(this)

        val toggle = ActionBarDrawerToggle(
            this,
            drawer_layout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        nav_view.setNavigationItemSelectedListener(this)

        receiveExtrasFromDetails()

        initPermissions()

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        RateMyApp(this).app_launched()
    }

    private fun receiveExtrasFromDetails() {
        latitude = intent.getDoubleExtra("LATITUDE", 0.0)
        longitude = intent.getDoubleExtra("LONGITUDE", 0.0)
    }
    
    private fun initMap() {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.mainContent, MapsFragment.newInstance(latitude, longitude))
            .addToBackStack(null)
            .commitAllowingStateLoss()
    }

    private fun initPermissions() {
        Log.d("Permisos", "dentro de initPermission")
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            makeRequest()
        } else {
            Log.d("Permisos", "dentro de initPermission else, Permiso concedido")
            initMap()

        }
    }

    private fun makeRequest() {
        Log.d("Permisos", "dentro de makeRequest")

        ActivityCompat.requestPermissions(
            this,
            arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE
        )
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
             when (requestCode) {
                 LOCATION_PERMISSION_REQUEST_CODE -> {
                     if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                         Log.d("Permisos", "onRequestPermissionsResult if")
                         basicAlert()
                     } else {
                         Log.d("Permisos", "onRequestPermissionsResult else")
                         initMap()
                     }
                 }
             }
         }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_home -> {
                replaceFragmenty(
                    fragment = MapsFragment(),
                    allowStateLoss = true,
                    containerViewId = R.id.mainContent
                )
                setTitle("Mapa")
            }
            R.id.nav_list -> {
//                val intent = Intent(this, TabsOrderByActivity::class.java)
//                startActivity(intent)

               /* val intent = TabsOrderByActivity.newIntent(this, listHome)
                startActivity(intent)*/


                replaceFragmenty(
                    fragment = TabsOrderByFragment(),
                    allowStateLoss = true,
                    containerViewId = R.id.mainContent
                )
                setTitle("Filtros")
            }
            R.id.nav_gas -> {
                val intent = Intent(this, GasPreferenceActivity::class.java)
                startActivity(intent)

            }
            R.id.nav_distance -> {
                val intent = Intent(this, DistancePreferenceActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_share -> {
                compartirTexto("http://play.google.com/store/apps/details?id="+getApplicationContext().getPackageName());

            }
            R.id.nav_send -> {
                compartirTexto("LISTA GASOLINAS: Gasolina  95, Gasolina 98, Diésel, Diésel Mejorado, Gasoleo B: Compartido por http://play.google/store/apps/details?id="+applicationContext.packageName);
            }
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }


    fun basicAlert() {
        val builder = android.app.AlertDialog.Builder(this)

        with(builder) {
            setTitle("Permiso Denegado")
            setMessage("Sin los permisos de localización la app se cerrará.")
            setPositiveButton("OK", DialogInterface.OnClickListener(function = positiveButtonClick))
            show()
        }
    }

    val positiveButtonClick = { dialog: DialogInterface, which: Int ->
        finish()
    }


    fun compartirTexto(texto: String){
        val intent = Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT,  texto);
        startActivity(Intent.createChooser(intent,"Selecciona aplicación"));
    }

}
