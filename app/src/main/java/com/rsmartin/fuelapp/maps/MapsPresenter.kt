package com.rsmartin.fuelapp.maps

import com.google.android.gms.maps.model.LatLng
import com.rsmartin.fuelapp.R
import com.rsmartin.fuelapp.Utils.Utils
import com.rsmartin.fuelapp.remote.data.ApiDataGob.ListaEESSPrecio
import com.rsmartin.fuelapp.remote.data.ListStations

class MapsPresenter(var mapsView: MapsFragment) {

    fun getDetailStation(markerLat: Double, markerLon: Double, listHome: List<ListaEESSPrecio>): ListStations {

        val station: ListStations = ListStations(
            R.drawable.marker_gas_station_default,
            "NAME", "ADDRESS", LatLng(0.0, 0.0), "cp", "town",
            "stateTown", 0, "horary",
            0.0, 0.0, 0.0, "0.0", 0.0,
            "lastUpdate", 0, 0, ListStations.ItemOrderBy.DISTANCE
        )

        for (list in listHome) {

            if (list.latitud != null && list.longitud != null &&
                Utils.replaceComaToDot(list.latitud).toString() == markerLat.toString() && Utils.replaceComaToDot(list.longitud).toString() == markerLon.toString()
            ) {

                station.name = list.rotulo
                station.address = list.direcciN
                if (list.precioGasolina95ProtecciN == null) {
                    list.precioGasolina95ProtecciN = "0,00"
                }
                station.price95 = Utils.replaceComaToDot(list.precioGasolina95ProtecciN)
                station.price98 = list.precioGasolina98
                if (list.precioGasoleoA == null) {
                    list.precioGasoleoA = "0,00"
                }
                station.priceGasOilA = Utils.replaceComaToDot(list.precioGasoleoA)
                if (list.precioNuevoGasoleoA == null) {
                    list.precioNuevoGasoleoA = "0,00"
                }
                station.priceNewGasOilA = Utils.replaceComaToDot(list.precioNuevoGasoleoA)
                station.priceGasOilB = list.precioGasoleoB
                station.horary = list.horario
                station.latlon = LatLng(Utils.replaceComaToDot(list.latitud), Utils.replaceComaToDot(list.longitud))
                station.cp = list.cp
                station.town = list.municipio
                station.stateTown = list.provincia

                return station
            }
        }
        return station
    }

}